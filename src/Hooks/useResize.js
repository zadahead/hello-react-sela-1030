import { useState, useEffect } from "react";


const useResize = (initialValue) => {
    const [value, setValue] = useState(initialValue || window.innerWidth);

    useEffect(() => {
        window.addEventListener('resize', onResize);
        return () => {
            window.removeEventListener('resize', onResize)
        }
    }, []);

    const onResize = (e) => {
        setValue(window.innerWidth);
        //console.log(window.innerWidth);
    }

    return {
        value,
        onResize
    };
}

export default useResize;