import { useEffect, useState } from 'react';
import axios from 'axios';

const useFetch = (url) => {
    const [list, setList] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);

    console.log('render', list);
    useEffect(() => {
        pullData();
    }, [])

    const pullData = async () => {
        try {
            const resp = await axios.get(url);
            setTimeout(() => {
                setList(resp.data)
            }, 2000);
        } catch (error) {
            setErrorMsg(error.message);
        }
    }

    return {
        list,
        errorMsg
    }
}

export default useFetch;