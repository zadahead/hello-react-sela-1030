import { combineReducers } from 'redux';


const countReducer = (count = 15, action) => {
    switch (action.type) {
        case 'ADD_COUNT':
            return count + action.payload;
        default:
            return count;
    }
}


const rootReducer = combineReducers({
    count: countReducer,
    mosh: () => { return 'ddd' }
})

export default rootReducer;
