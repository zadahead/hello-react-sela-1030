

export const addCount = () => {
    return {
        type: 'ADD_COUNT',
        payload: 1
    }
}