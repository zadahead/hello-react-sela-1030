import { createStore } from "redux";
import rootReducer from "State/Reducers";

const store = createStore(rootReducer);

export default store;