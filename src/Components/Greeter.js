import GoodBye from "./GoodBye";
import Hello from "./Hello";

const Greeter = (props) => {
    if (props.isLoggedIn) {
        return <Hello />
    }
    return <GoodBye />
}

export default Greeter;