
const Numbers = (props) => {
    console.log('Numbers => ', props);

    const renderList = () => {
        return props.list.map((item) => {
            return (
                <div key={item}>
                    <h3>{item}</h3>
                </div>
            )
        })
    }

    return (
        <div>
            <h2>Numbers</h2>
            {renderList()}
        </div>
    )
}

export default Numbers;