import User from './User';

const UsersList = (props) => {

    const renderList = () => {
        return props.list.map((item, index) => {
            return <User key={index} user={item} />
        })
    }


    return (
        <div>
            <h2>UsersList</h2>
            {renderList()}
        </div>
    );
}

export default UsersList;