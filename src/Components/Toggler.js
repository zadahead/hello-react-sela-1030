import { useState } from 'react';
import FuncLifeCycle from './FuncLifeCycle';

const Toggler = () => {
    const [isOn, setIsOn] = useState(true);

    const clickHandler = () => {
        setIsOn(!isOn);
    }

    const renderHello = () => {
        if (isOn) {
            return <FuncLifeCycle />;
        }
        //undefined
    }

    return (
        <div>
            <div>
                {renderHello()}
            </div>
            <div>
                <button onClick={clickHandler}>Click Here</button>
            </div>
        </div>
    )
}

export default Toggler;