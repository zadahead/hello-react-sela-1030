import { connect } from "react-redux";


const CounterDisplay = (props) => {
    console.log("props", props);

    return <h2>CounterDisplay, {props.count}</h2>
}

const mapStoreToProps = ({ count }) => {
    return { count };
}

export default connect(mapStoreToProps)(CounterDisplay);