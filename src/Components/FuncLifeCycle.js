import { useState, useEffect } from 'react';

const FuncLifeCycle = () => {
    const [count, setCount] = useState(0);
    const [count2, setCount2] = useState(0);
    const [isBlue, setIsBlue] = useState(false);

    //did mount
    useEffect(() => {
        window.addEventListener('click', handleScreenClick);

        //unmount
        return () => {
            window.removeEventListener('click', handleScreenClick);
        }
    }, [])

    //did update
    useEffect(() => {
        console.log('curr count2 changed or mounted', count2);
    }, [count2])


    const handleScreenClick = () => {
        //alert('hello!!')
        console.log('handleScreenClick');
    }
    const handleChangeColor = () => {
        setIsBlue(!isBlue);
    }

    const handleAddNumber = () => {
        setCount2(count2 + 1);
    }


    const style = { color: isBlue ? 'blue' : 'red' };

    return (
        <div>
            <h2 style={style}>FuncLifeCycle, count: {count2}</h2>
            <button onClick={handleChangeColor}>Change Color</button>
            <button onClick={handleAddNumber}>Add Number</button>
        </div>
    )
}

export default FuncLifeCycle;