import React from "react";

class Counter extends React.Component {
    //state 
    state = {
        count: 0
    }

    //handlers
    handleClick = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    //render
    render = () => {
        return <h1 onClick={this.handleClick}>Counter, count: {this.state.count}</h1>
    }
}

export default Counter;