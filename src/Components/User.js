import { useState } from "react";

const User = ({ user }) => {
    const [isVis, setVis] = useState(false);

    const clickHendler = () => {
        setVis(!isVis);
    }

    const renderNickname = () => {
        if (isVis) {
            return `${user.nickname} -hide-`;
        }
        return `-show`;
    }

    return (
        <h2>
            <span>#{user.id}: {user.name}, </span>
            <span onClick={clickHendler}>({renderNickname()})</span>
        </h2>
    )



}

export default User;