import { Rows, Input, Btn } from "UIKit";

import { useState } from "react";

const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleSetUserName = (e) => {
        setUsername(e.target.value);
    }

    const handleSetPassword = (e) => {
        setPassword(e.target.value);
    }

    const handleLogin = () => {
        console.log('handleLogin', username, password);
    }

    return (
        <Rows>
            <Input i="user" placeholder="username..." value={username} onChange={handleSetUserName} />
            <Input i="key" placeholder="password..." type="password" value={password} onChange={handleSetPassword} />
            <Btn i="user" onClick={handleLogin}>Click Me</Btn>
        </Rows>
    )
}

export default Login;