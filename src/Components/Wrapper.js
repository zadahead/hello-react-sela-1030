const Wrapper = (props) => {
    const style = {
        border: '1px solid black',
        borderRadius: '25px',
        padding: '10px'
    };

    return (
        <div style={style}>
            <h1>{props.header}</h1>
            <div style={{ backgroundColor: 'black', color: 'white', style }}>
                {props.children}
            </div>
        </div>
    )
}

export default Wrapper;