import { useState } from 'react';

const CounterFunctional = () => {
    const [count, setCount] = useState(0);
    const [isBlue, setColor] = useState(true);

    const clickHandler = () => {
        if (isBlue) {
            setCount(count + 1);
        }
        setColor(!isBlue);
    }
    return <h2 style={{ color: isBlue ? "blue" : "red" }} onClick={clickHandler}>Counter: {count}</h2>

}

export default CounterFunctional;