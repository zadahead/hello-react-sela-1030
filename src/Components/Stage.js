import { useState, useEffect } from "react";


const Stage = (props) => {
    console.log(props);

    //state
    const [count, setCount] = useState(props.initialCount !== null ? props.initialCount : 0);
    const [isBlue, setIsBlue] = useState(props.initialColor !== null ? props.initialColor : true);

    //life cycle 
    useEffect(() => {
        //did mount
        if (props.msg) {
            window.addEventListener('click', handleWindowClick);
        }

        return () => {
            window.removeEventListener('click', handleWindowClick);
        }
    }, [])

    useEffect(() => {
        if (props.initialCount !== count) {
            setCount(props.initialCount || 0)
        }
    }, [props.initialCount])

    useEffect(() => {
        if (props.initialColor !== null && props.initialColor !== isBlue) {
            setIsBlue(props.initialColor)
        }
    }, [props.initialColor])

    //events
    const handleAddCount = () => {
        setCount(count + 1);
    }

    const handleChangeColor = () => {
        setIsBlue(!isBlue);
    }

    const handleWindowClick = () => {
        if (props.msg) {
            console.log('alert!!', props.msg);
        }
    }

    //render
    const styleCss = { color: isBlue ? 'blue' : 'red' }

    return (
        <div>
            <div>
                <h2 style={styleCss}>Count: {count}</h2>
                <h3>{props.msg}</h3>
                <button onClick={handleAddCount}>Add</button>
                <button onClick={handleChangeColor}>Change Color</button>
            </div>
        </div>
    )
}

export default Stage;