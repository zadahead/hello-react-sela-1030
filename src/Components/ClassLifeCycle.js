import React from 'react';

class ClassLifeCycle extends React.Component {
    state = {
        count: 0
    }

    componentDidMount = () => {
        console.log('componentDidMount');
    }

    componentDidUpdate = () => {
        console.log('componentDidUpdate');
    }

    componentWillUnmount = () => {
        console.log('componentWillUnmount');
    }

    handleClick = () => {
        console.log('setState');
        this.setState({
            count: this.state.count + 1
        })
    }

    render = () => {
        console.log('render');

        return <h2 onClick={this.handleClick}>ClassLifeCycle, count: {this.state.count}</h2>
    }
}

export default ClassLifeCycle;