import { Btn } from 'UIKit';

import { connect } from "react-redux";
import { addCount } from 'State/Actions';

const CounterAdd = (props) => {
    return <Btn onClick={props.addCount}>Add</Btn>
}

export default connect(null, { addCount })(CounterAdd);