

import { Rows, Btn, Input } from 'UIKit';

import useInput from 'Hooks/useInput';
import useResize from 'Hooks/useResize';

const Hooks = () => {
    const username = useInput('Mosh');
    const password = useInput();

    const resize = useResize();


    const handleGetValue = () => {
        console.log(username.value, password.value);
    }

    return (
        <Rows>
            <h1>Hooks</h1>
            <h2>window width: {resize.value}</h2>
            <Input {...username} placeholder="username" />
            <Input {...password} placeholder="password" type="password" />
            <Btn onClick={handleGetValue}>Get Values</Btn>
        </Rows>
    )
}

export default Hooks;

