import CounterDisplay from 'Components/CounterDisplay';
import CounterAdd from 'Components/CounterAdd';

import { Line } from 'UIKit';
const Redux = () => {
    return (
        <div>
            <h2>Redux</h2>
            <Line>
                <CounterDisplay />
                <CounterAdd />
            </Line>
        </div>
    )
}

export default Redux;