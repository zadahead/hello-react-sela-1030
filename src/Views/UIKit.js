import { Route, Link, NavLink } from 'react-router-dom';

import { Sidebar, Rows } from "UIKit";

import Counter from 'Components/Counter';
import Hello from 'Components/Hello';

const UIKit = () => {
    return (
        <Sidebar>
            <div>
                <Rows>
                    <Link to="btn">Btn</Link>
                    <Link to="input">Input</Link>
                </Rows>
            </div>
            <div>
                <Route path="/uikit/btn" component={Counter}></Route>
                <Route path="/uikit/input" component={Hello}></Route>
            </div>
        </Sidebar>
    )
}

export default UIKit;