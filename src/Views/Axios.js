

import useFetch from "Hooks/useFetch";

const Axios = () => {
    const { list, errorMsg } = useFetch('https://jsonplaceholder.typicode.com/todos');

    if (errorMsg) {
        return <h2>Error: {errorMsg}</h2>
    }
    else if (list) {
        return <h2>Results: {list.length}</h2>
    } else {
        return 'loading...'
    }
}

export default Axios;