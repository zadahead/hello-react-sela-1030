import { useState } from 'react';
import { Box, Form, Dropdown } from 'UIKit';


const list = [
    { id: 1, value: 'mosh' },
    { id: 2, value: 'david' },
    { id: 3, value: 'baruch' }
]
const ViewDropdown = () => {
    const [selected, setSelected] = useState(null);

    console.log('RENDER!!', selected);

    const handleOnChange = (item) => {
        console.log('handleOnChange', item);
        setSelected(item.id);
    }
    return (
        <Box>
            <Form
                header="DropDown"
                info="This is a great dropdown"
            >
                <Dropdown list={list} onChange={handleOnChange} selected={selected} />
            </Form>
        </Box>
    )
}

export default ViewDropdown;