import './Btn.css';
import { Line, Icon } from 'UIKit';

const Btn = (props) => {
    return (
        <div className="Btn" onClick={props.onClick}>
            <Line justify="between">
                <div>{props.children}</div>
                {props.i !== undefined ? <Icon i={props.i} /> : undefined}
            </Line>
        </div>
    )
}

export default Btn;