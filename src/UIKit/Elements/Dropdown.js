import { useEffect, useState, useRef } from 'react';

import { Line, Icon } from 'UIKit';
import './Dropdown.css';

const Dropdown = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const dropdownContainer = useRef();

    useEffect(() => {
        window.addEventListener('click', onCloseIsOpen);

        return () => {
            window.removeEventListener('click', onCloseIsOpen);
        }
    }, [])

    const handleHeaderClick = () => {
        console.log('handleHeaderClick', !isOpen);
        setIsOpen(!isOpen)
    }

    const handleItemClick = (item) => {
        if (props.onChange) {
            props.onChange(item);
        }
        onCloseIsOpen();
    }

    const onCloseIsOpen = (e) => {
        console.log('onCloseIsOpen');
        if (dropdownContainer?.current?.contains(e?.target)) {

        } else {
            setIsOpen(false);
        }
    }

    const renderList = () => {
        if (isOpen && props.list) {
            return (
                <div className="list">
                    {renderItems()}
                </div>
            )
        }
    }

    const renderItems = () => {
        //{ id: 1, value: 'sdsd' }
        return props.list.map(i => {
            return <div key={i.id} onClick={() => { handleItemClick(i) }}>{i.value}</div>
        })
    }

    const renderHeader = () => {
        if (props.selected) {
            const selected = props.list.find(i => i.id === props.selected);
            if (selected) {
                return <div>{selected.value}</div>
            }
        }

        return <div>Please Select</div>
    }
    return (
        <div className="Dropdown" ref={dropdownContainer}>
            <div className="header" onClick={handleHeaderClick}>
                <Line>
                    {renderHeader()}
                    <Icon i={isOpen ? `chevron-up` : `chevron-down`} />
                </Line>
            </div>
            {renderList()}
        </div>
    )
}

export default Dropdown;