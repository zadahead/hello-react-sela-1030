import "./Input.css";

import { Line, Icon } from "UIKit";

const Input = (props) => {
    return (
        <div className="Input">
            <Line className="no-padding-left">
                <input
                    value={props.value}
                    onChange={props.onChange}
                    type={props.type || 'text'}
                    placeholder={props.placeholder}
                />
                {props.i ? <Icon i={props.i} /> : ''}
            </Line>
        </div>
    )
}

export default Input;