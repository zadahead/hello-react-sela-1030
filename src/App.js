
import 'App.css';
import { Main, Line, Icon } from 'UIKit';

import { Route, Link, NavLink, Switch, Redirect } from 'react-router-dom';

import { useState } from 'react';

import Home from 'Views/Home';
import About from 'Views/About';
import UIKit from 'Views/UIKit';
import ViewDropdown from 'Views/ViewDropdown';
import Hooks from 'Views/Hooks';
import Axios from 'Views/Axios';
import Redux from 'Views/Redux';


const App = () => {
    const [count, setCount] = useState(0);
    const handleClick = () => {
        setCount(count + 1);
    }

    return (
        <Main>
            <Line justify="between">
                <Line>
                    <Icon i="heart" />
                    <Icon i="star" />
                    <div>logo</div>
                </Line>
                <Line>
                    <NavLink to="/home">Home</NavLink>
                    <NavLink to="/dropdown">Dropdown</NavLink>
                    <NavLink to="/hooks">Hooks</NavLink>
                    <NavLink to="/axios">Axios</NavLink>
                    <NavLink to="/redux">Redux</NavLink>
                </Line>
            </Line>
            <div>
                <Switch>
                    <Route exact path="/" component={Home}></Route>
                    <Route path="/about" component={About}></Route>
                    <Route path="/uikit" component={UIKit}></Route>
                    <Route path="/dropdown" component={ViewDropdown}></Route>
                    <Route path="/hooks" component={Hooks}></Route>
                    <Route path="/axios" component={Axios}></Route>
                    <Route path="/redux" component={Redux}></Route>

                    <Redirect to='/' />
                </Switch>
            </div>
        </Main>
    )
}

export default App;

